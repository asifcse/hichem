import { UsersService } from './../../../services/users/users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  users: any;
  constructor(private userServ: UsersService) {

  }

  ngOnInit() {
    this.userServ.getUsersList().subscribe((res: any) => {
     
      this.users = res.body.results;
      console.log(this.users);

    });
  }

}
