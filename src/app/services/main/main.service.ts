import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, retry } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private http: HttpClient) { }


  getData(URI): Observable<any> {
    return this.http.get(URI, { observe: 'response' }).pipe(
      catchError(this.handleError())
    );
  }

  postData(URI): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        // Authorization: 'Bearer ' + this.TOKEN_VALUE
      })
    };
    return this.http.post(URI, { observe: 'response' }, httpOptions).pipe(
      catchError(this.handleError())
    );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}


