import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../main/main.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private mainServ: MainService) {

  }

  getUsersList() {
    return  this.mainServ.getData('https://randomuser.me/api/');
  }
}
