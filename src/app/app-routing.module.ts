import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'onboard',
    loadChildren: () => import('./pages/authentications/onboarding/onboarding.module').then( m => m.OnboardingModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/members/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'school',
    loadChildren: () => import('./pages/authentications/school-key/school-key.module').then( m => m.SchoolKeyModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/authentications/login/login.module').then( m => m.LoginModule)
  },
  {
    path: '',
    redirectTo: 'onboard',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
